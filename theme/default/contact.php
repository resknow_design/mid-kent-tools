<?php

// Meta data example
$this->page = array(

    // Page title
    'title' => 'Contact Us | '. SITE_NAME,

    // Page description
    'description' => "We supply tools and fixings to Maidstone and surrounding areas. We have over 5000 products in stock
and If we don't have what you want we can get it. We have cracking deals on many of our products instore and
Online. So come on and have a look, at Mid Kent Tools and fixings we welcome all.",

    // Page keywords
    'keywords' => "Mid Kent Tools and Fixings, tools maidstone, power tools maidstone, Safety Equipment Maidstone,
    sealants maidstone, tool shop maidstone, hand tools maidstone, tools Aylesford, power tools Aylesford,
    Safety Equipment Aylesford, sealants Aylesford, tool shop Aylesford, hand tools Aylesford",

    // Canonical URL, this can be a string or just leave as is for current url.
    'canonical' => $this->path

);

// Get header
$this->get_header();

?>

<!-- Your page content goes here. Copy and rename this file to create other pages. -->



<div class="row">
  <div class="container">
    <div class="col-md-8 welcome">
      <h2>Feel free to pay us a visit</h2>
      <div class="col-md-6">
        <div class="map-container">
          <div id="map-canvas" data-lat="51.398877" data-lng="0.513711"></div>
        </div>
        <p class="address">Unit 4-5/Keel Court Enterprise Close<br>Medway City Estate<br>Rochester<br>ME2 4LY<br>01634 290 567</p>
      </div>

      <div class="col-md-6">
        <div class="map-container">
          <div id="map-canvas-2" data-lat="51.380152" data-lng="0.477404"></div>
        </div>
        <p class="address">Unit 3 Brookhouse Larkfield Trading Estate<br>New Hythe Lane<br>Larkfield<br>ME20 6GN<br>01622 717 766</p>
      </div>

    </div>

    <div class="col-md-4">
      <div class="ebay-store">
        <h3>Browse our eBay Store</h3>
        <a href="http://www.ebay.co.uk/usr/jsonlinetools"><img src="<?php assets_dir(); ?>/images/ebay-logo.png"></a>
      </div>
      <div class="contact">
        <h2>Quick Contact Form</h2>

                    <div id="notify"></div>
                      <form method="post" action="#" id="contact">
                        <input id="name" type="text" placeholder="Name" name="name">
                        <input id="phone" type="tel" placeholder="Phone" name="phone">
                        <input id="email" type="email" placeholder="Email Address" name="email">
                        <textarea id="message" placeholder="Message" name="message"></textarea>
                        <input style="position: absolute; top: 0; left: -9999px;" id="subject" type="text" placeholder="Subject" name="subject">
                        <button id="submit" type="button">SEND</button>
                      </form>





        <!--  <form method="post" action="#" id="">
              <input type="text" placeholder="Name" name="name">
              <input type="text" placeholder="Email" name="email">
              <input type="text" placeholder="Contact Number" name="contact number">
              <textarea placeholder="Message" name="message"></textarea>
              <button>SEND</button>
          </form> -->
      </div>
    </div>
  </div>
</div>


<?php /* Get footer */ $this->get_footer(); ?>
