<?php

// Meta data example
$this->page = array(

    // Page title
    'title' => 'Services | '. SITE_NAME,

    // Page description
    'description' => "We supply tools and fixings to Maidstone and surrounding areas. We have over 5000 products in stock
and If we don't have what you want we can get it. We have cracking deals on many of our products instore and
Online. So come on and have a look, at Mid Kent Tools and fixings we welcome all.",

    // Page keywords
    'keywords' => "Mid Kent Tools and Fixings, tools maidstone, power tools maidstone, Safety Equipment Maidstone,
    sealants maidstone, tool shop maidstone, hand tools maidstone, tools Aylesford, power tools Aylesford,
    Safety Equipment Aylesford, sealants Aylesford, tool shop Aylesford, hand tools Aylesford",

    // Canonical URL, this can be a string or just leave as is for current url.
    'canonical' => $this->path

);

// Get header
$this->get_header();

?>

<!-- Your page content goes here. Copy and rename this file to create other pages. -->



<div class="row">
  <div class="container">
    <div class="col-md-8 welcome">

      <h2>Services</h2>
        <p>As a leading supplier of tools &amp; fixings, Mid Kent offers a friendly service, wide range of goods and free local delivery! We also offer clients a sales &amp; workshop service, as our staff are extremely knowledgeable and always strive to help with client needs and requirements. </p>


<!-- 4:3 aspect ratio -->
<div class="embed-responsive embed-responsive-4by3">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sb1StnUJZNU" frameborder="0" allowfullscreen></iframe>
</div>

    </div>

    <div class="col-md-4">
      <div class="contact">
        <div class="ebay-store">
          <h3>Browse our eBay Store</h3>
          <a href="http://www.ebay.co.uk/usr/jsonlinetools"><img src="<?php assets_dir(); ?>/images/ebay-logo.png"></a>
        </div>
        <h2>Quick Contact Form</h2>
        <div id="notify"></div>
          <form method="post" action="#" id="contact">
            <input id="name" type="text" placeholder="Name" name="name">
            <input id="phone" type="tel" placeholder="Phone" name="phone">
            <input id="email" type="email" placeholder="Email Address" name="email">
            <textarea id="message" placeholder="Message" name="message"></textarea>
            <input style="position: absolute; top: 0; left: -9999px;" id="subject" type="text" placeholder="Subject" name="subject">
            <button id="submit" type="button">SEND</button>
          </form>
      </div>
    </div>
  </div>
</div>


<?php /* Get footer */ $this->get_footer(); ?>
