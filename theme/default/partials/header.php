<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $this->page['title']; ?></title>
    <meta name="description" content="<?php echo $this->page['description']; ?>">
    <meta name="keywords" content="<?php echo $this->page['keywords']; ?>">
    <link rel="canonical" href="<?php echo $this->domain .'/'. $this->page['canonical']; ?>">

    <link rel="stylesheet" type="text/css" href="<?php assets_dir() ?>/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php assets_dir() ?>/images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php assets_dir() ?>/images/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php assets_dir() ?>/images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php assets_dir() ?>/images/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php assets_dir() ?>/images/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">

    <?php if ( part_exists('analytics') ): $this->get_partial('analytics'); endif; ?>

    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php assets_dir() ?>/css/ie.css">
    <![endif]-->

</head>
<body class="page-<?php echo $this->slug; ?>">

    <?php /* Browser Update Message */ if ( part_exists('analytics') ): $this->get_partial('browser-update'); endif; ?>

<div class="row">
  <div class="container address-container">
    <div class="address-left"><p>Unit 4-5/Keel Court Enterprise Close<br>
                                Medway City Estate<br>
                                Rochester<br>
                                ME2 4LY<br>
                                01634290567</p></div>
    <div class="logo"><img src="<?php assets_dir(); ?>/images/logo.png"></div>
    <div class="address-right"><p>Unit 3 Brookhouse Larkfield Trading Estate<br>
                                New Hythe Lane<br>
                                Larkfield<br>
                                ME20 6GN<br>
                                01622717766<br>
                                07890810305</div>
  </div>
</div>

<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/">Home</a></li>
        <li><a href="services">Services</a></li>
        <li><a href="gallery">Gallery</a></li>
        <li><a href="contact">Contact Us</a></li>
      </ul>




    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
