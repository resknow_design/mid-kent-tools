<?php

// Meta data example
$this->page = array(

    // Page title
    'title' => 'Tools and Fixings, Kent | '. SITE_NAME,

    // Page description
    'description' => "We supply tools and fixings to Maidstone and surrounding areas. We have over 5000 products in stock
and if we don't have what you want we can get it. We have cracking deals on many of our products instore and
Online. So come on and have a look, at Mid Kent Tools and fixings we welcome all.",

    // Page keywords
    'keywords' => 'Mid Kent Tools and Fixings, tools maidstone, power tools maidstone, Safety Equipment Maidstone,
    sealants maidstone, tool shop maidstone, hand tools maidstone, tools Aylesford, power tools Aylesford,
    Safety Equipment Aylesford, sealants Aylesford, tool shop Aylesford, hand tools Aylesford',

    // Canonical URL, this can be a string or just leave as is for current url.
    'canonical' => $this->path

);

// Get header
$this->get_header();

?>

<!-- Your page content goes here. Copy and rename this file to create other pages. -->



<div class="row">
  <div class="container">
    <div class="col-md-3 col-sm-6 hand-tools">
      <h3>Hand Tools</h3>
      <img src="<?php assets_dir(); ?>/images/img1.gif">
      <p>We provide a wide range of hand tools, from hammers to screw drivers and everything in-between. We will always do our best to get the products you want, so if there’s anything you can’t see already give us a nudge and we’ll work our magic!</p>
      <a href="http://www.ebay.co.uk/usr/jsonlinetools">BROWSE STORE</a>
    </div>

    <div class="col-md-3 col-sm-6 hand-tools">
      <h3>Power Tools</h3>
      <img src="<?php assets_dir(); ?>/images/img2.gif">
      <p>We have a large stock of power tools, from drills to power saws. We do our best to get what you need fast, so that you can get the job done.</p>
      <a href="http://www.ebay.co.uk/usr/jsonlinetools">BROWSE STORE</a>
    </div>

    <div class="col-md-3 col-sm-6 hand-tools">
      <h3>Safety Equipment</h3>
      <img src="<?php assets_dir(); ?>/images/img3.gif">
      <p>You will find all the safety equipment you will require for you business on our eBay or in store at a low price. Explore our safety equipment today to find exactly what you’re looking for.</p>
      <a href="http://www.ebay.co.uk/usr/jsonlinetools">BROWSE STORE</a>
    </div>

    <div class="col-md-3 col-sm-6 hand-tools">
      <h3>Fein Products</h3>
      <img src="<?php assets_dir(); ?>/images/img4.gif">
      <p>FEIN is the expert when it comes to professional, efficient and extremely reliable power tools and special application solutions, which is why we stock them here at Mid Kent Tools &amp; Fixings.</p>
      <a href="http://www.ebay.co.uk/usr/jsonlinetools">BROWSE STORE</a>
    </div>
  </div>
</div>

<div class="row">
  <div class="container welcome-container">
    <div class="col-md-8 welcome">
      <h1>Welcome to Mid Kent Tools &amp; Fixings</h1>

      <p>We supply tools and fixings to Maidstone, West Malling, Chatham and surrounding areas. We have over 5000 products in stock and if we don't have what you want we can get it. We have cracking deals on many of our products in store and online. So come on in and have a look – at Mid Kent Tools and fixings we welcome everyone.</p>

        <div class="col-md-6">
          <div class="brand-logo"><img src="<?php assets_dir(); ?>/images/logo-bosch.png"></div>
        </div>

        <div class="col-md-6">
          <div class="brand-logo"><img src="<?php assets_dir(); ?>/images/logo-dewalt.png"></div>
        </div>

        <div class="col-md-6">
          <div class="brand-logo"><img src="<?php assets_dir(); ?>/images/logo-festool.png"></div>
        </div>

        <div class="col-md-6">
          <div class="brand-logo"><img src="<?php assets_dir(); ?>/images/logo-makita.png"></div>
        </div>

    </div>

    <div class="col-md-4">
      <div class="ebay-store">
        <h3>Browse our eBay Store</h3>
        <a href="http://www.ebay.co.uk/usr/jsonlinetools"><img src="<?php assets_dir(); ?>/images/ebay-logo.png"></a>
      </div>
        <div class="contact">
          <h2>Quick Contact Form</h2>
          <div id="notify"></div>
            <form method="post" action="#" id="contact">
              <input id="name" type="text" placeholder="Name" name="name">
              <input id="phone" type="tel" placeholder="Phone" name="phone">
              <input id="email" type="email" placeholder="Email Address" name="email">
              <textarea id="message" placeholder="Message" name="message"></textarea>
              <input style="position: absolute; top: 0; left: -9999px;" id="subject" type="text" placeholder="Subject" name="subject">
              <button id="submit" type="button">SEND</button>
            </form>
        </div>

    </div>
  </div>
</div>


<?php /* Get footer */ $this->get_footer(); ?>
